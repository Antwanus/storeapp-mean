// Main server entry point
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');
const mongoose = require('mongoose');
const config = require('./config/db')
const users = require('./routes/users');


// let's get cooking :D
mongoose.connect(config.database, 
    // add options to avoid deprecation
    {
    useUnifiedTopology: true,
    useNewUrlParser: true
    }
);
mongoose.connection.on('connected', () => {
    console.log('Connected to MongoDB: ' + config.database);
});
mongoose.connection.on('error', (err) => {
    console.log('ERROR CONNECTING TO MONGOLDB: ' + err);
});

const app = express();
const port = 3003;

// allow X-realm (MW)
app.use(cors());
// parse body of the responses into JSON (MW)
app.use(bodyParser.json());
// use routes.users for /users
app.use('/users', users);
// set static folder (for angular app)
app.use(express.static(path.join(__dirname, 'client')));
// passport MW (www.passportjs.org)
app.use(passport.initialize());
app.use(passport.session());
require('./config/passport')(passport);

// index route
app.get('/', (req, res) => {
    res.send('Welcome human');
});

// on CLI => npm start (or nodemon for auto updates <3), listen on port #
app.listen(port, () => {
    console.log('Server started on port ' + port); 
});