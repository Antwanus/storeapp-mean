import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { MatPaginatorModule } from "@angular/material";
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from "@angular/material/table";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatStepperModule } from '@angular/material/stepper';
import { MatInputModule } from "@angular/material";
import { MatDialogModule } from "@angular/material/dialog";
import { MatButtonModule } from "@angular/material/button";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { NgFlashMessagesModule } from "ng-flash-messages";

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { JwtModule } from "@auth0/angular-jwt";

import { ProductListComponent } from './components/product-list/product-list.component';
import { NavbarComponent } from './header/navbar/navbar.component';
import { FooterComponent } from './footer/footer/footer.component';
import { WelcomeComponent } from './components/pages/welcome/welcome.component';
import { SupplierListComponent } from './components/supplier-list/supplier-list.component';
import { CustomerListComponent } from './components/customer-list/customer-list.component';
import { ProductFormComponent } from './components/product-form/product-form.component';
import { CustomerFormComponent } from './components/customer-form/customer-form.component';
import { SupplierFormComponent } from './components/supplier-form/supplier-form.component';
import { RegisterComponent } from './components/pages/register/register.component';
import { LoginComponent } from './components/pages/login/login.component';
import { DashboardComponent } from './components/pages/dashboard/dashboard.component';

import { ValidateService } from './services/validate.service';
import { AuthService } from './services/auth.service';
import { ProfileComponent } from './components/pages/profile/profile.component';

import { AuthGuard } from './services/guards/Auth.guard';
import { InterceptorServiceService } from './services/interceptors/interceptor-service.service';

export function tokenGetter() {
  return localStorage.getItem("id_token");
}

@NgModule({
  declarations: [
    AppComponent,
    ProductListComponent,
    NavbarComponent,
    FooterComponent,
    WelcomeComponent,
    SupplierListComponent,
    CustomerListComponent,
    ProductFormComponent,
    CustomerFormComponent,
    SupplierFormComponent,
    RegisterComponent,
    LoginComponent,
    DashboardComponent,
    ProfileComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    
    NgFlashMessagesModule.forRoot(),
    NgbModule,
    MatTableModule,
    MatSortModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatButtonModule,
    MatDialogModule,
    MatInputModule,
    MatCheckboxModule,
    MatStepperModule,

    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    HttpClientModule,
    JwtModule.forRoot({
        config: {
          tokenGetter: tokenGetter,
          whitelistedDomains: ["localhost:3003", "localhost:8080"],
          blacklistedRoutes: ["example.com/examplebadroute/"]
        }
    }),
  ],
  exports: [NgbModule],
  providers: [
    ValidateService,
    AuthService,
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorServiceService,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }