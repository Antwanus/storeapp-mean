import { Component, HostListener, ElementRef } from '@angular/core';
import { faAngleDoubleUp } from "@fortawesome/free-solid-svg-icons";
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})


export class FooterComponent {
  faAngleDoubleUp = faAngleDoubleUp;
  isVisible: boolean;
  topPosToStartShowing = 100;

  @HostListener('window:scroll')
  checkScroll() {

    const scrollPosition = 
              window.pageYOffset ||
              document.documentElement.scrollTop || 
              document.body.scrollTop || 0;

    //console.log('[scroll]', scrollPosition);

    if (scrollPosition >= this.topPosToStartShowing) {
      this.isVisible = true;
    } else {
      this.isVisible = false;
    }
  }

  gotoTop() {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }
}
