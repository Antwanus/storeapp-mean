export class Supplier {
    supplierName?: string;
    supplierId?: number;
    bankAccount?: [
        {
            accountHolder?: string;
            accountNumber?: number;
        }
    ];
    _links?: {
        self?: {
            href?: string;
        },
        supplier?: {
            href?: string;
        },
        addresses?: {
            href?: string;
        },
        products?: {
            href?: string;
        },
        bankaccounts?: {
            href?: string;
        }
    }
}