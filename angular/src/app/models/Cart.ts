export class Cart {
    created?: string;
    completed?: boolean;
    _links?: {
        self?: {
            href: string;
        },
        cart?: {
            href: string;
        },
        products?: {
            href: string;
        },
        customer?: {
            href: string;
        }
    }
}