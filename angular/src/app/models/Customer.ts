export class Customer {
    customerName: string;
    bankAccount?: [
        {
            accountHolder?: string;
            accountNumber?: string;
        }
    ];
    customerid?: number;
    _links?: {
        self?: {
            href: string;
        },
        customer?: {
            href: string;
        },
        addresses?: {
            href: string;
        },
        carts?: {
            href: string;
        },
        bankAccounts?: {
            href: string;
        }
    };
}