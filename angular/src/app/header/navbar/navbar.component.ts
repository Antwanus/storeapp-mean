import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgFlashMessageService } from 'ng-flash-messages';

import { AuthService } from 'src/app/services/auth.service';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private router: Router,
    private flashMsg: NgFlashMessageService,

  ) { }

  ngOnInit() {
  }

  onLogout(){
    this.authService.logout();
    
    this.router.navigate(['/']);
    this.flashMsg.showFlashMessage({
      messages: ['You are logged out'],
      dismissible: true,
      timeout: false,
      type: 'danger'
    });
    return false;
  }
}
