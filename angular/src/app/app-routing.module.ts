import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WelcomeComponent } from './components/pages/welcome/welcome.component';
import { RegisterComponent } from './components/pages/register/register.component';
import { LoginComponent } from './components/pages/login/login.component';

import { ProductListComponent } from './components/product-list/product-list.component';
import { ProductFormComponent } from './components/product-form/product-form.component';

import { SupplierListComponent } from './components/supplier-list/supplier-list.component';
import { SupplierFormComponent } from './components/supplier-form/supplier-form.component';

import { CustomerListComponent } from './components/customer-list/customer-list.component';
import { CustomerFormComponent } from './components/customer-form/customer-form.component';
import { DashboardComponent } from './components/pages/dashboard/dashboard.component';
import { ProfileComponent } from './components/pages/profile/profile.component';
import { AuthGuard } from './services/guards/Auth.guard';

const routes: Routes = [
  { 
    path: "", 
    component: WelcomeComponent 
  },
  { 
    path: "product-list", 
    component: ProductListComponent,
    canActivate: [AuthGuard]  
  },
  { 
    path: "product-form", 
    component: ProductFormComponent,
    canActivate: [AuthGuard] 
  },
  { 
    path: "supplier-list", 
    component: SupplierListComponent,
    canActivate: [AuthGuard]  
  },
  { 
    path: "supplier-form", 
    component: SupplierFormComponent,
    canActivate: [AuthGuard]  
  },
  { 
    path: "customer-list", 
    component: CustomerListComponent,
    canActivate: [AuthGuard]  
  },
  { 
    path: "customer-form", 
    component: CustomerFormComponent,
    canActivate: [AuthGuard]  
  },
  { 
    path: "dashboard", 
    component: DashboardComponent,
    canActivate: [AuthGuard]  
  },
  { 
    path: "register", 
    component: RegisterComponent 
  },
  {
    path: "login",
    component: LoginComponent
  },
  { 
    path: "profile", 
    component: ProfileComponent,
    canActivate: [AuthGuard] 
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
