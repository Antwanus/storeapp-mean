import { Component, OnInit } from '@angular/core';
import { CustomerService } from "../../services/customer.service";
import { Customer } from "../../models/Customer";

import { FormBuilder, FormGroup } from "@angular/forms";
import { NgFlashMessageService } from 'ng-flash-messages';

@Component({
  selector: 'app-customer-form',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css']
})
export class CustomerFormComponent implements OnInit {

  customer: Customer;
  customerForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private customerService: CustomerService, 
    private flashMsg: NgFlashMessageService,
  ) { 
    this.customerForm = this.formBuilder.group({
      customerName: ''
    });
  }

  ngOnInit() {  }
  
  ngAfterContentInit() {
    // welcome & instruct user
    this.flashMsg.showFlashMessage({
      messages: ['Please fill in the form.'],
      dismissible: true,
      timeout: 5000,
      type: 'info'
    });
  }
  // SUBMIT FORM
  onSubmit() {
    // wire customer to formdata
    this.customer = this.customerForm.value;
    // reset form values
    this.customerForm.reset();
    // handle POST request
    this.customerService
            .createCustomer(this.customer)
            .subscribe();
    //TODO replace with custom flashmsg & maybe use the response to return the new ID of the product
    window.alert("Sending data to the DB...");
  }

}
