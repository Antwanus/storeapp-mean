import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from "@angular/router";

import { faSmileWink } from "@fortawesome/free-solid-svg-icons";
import { NgFlashMessageService } from 'ng-flash-messages';

import { ValidateService } from 'src/app/services/validate.service';
import { AuthService } from 'src/app/services/auth.service';
import { User } from 'src/app/models/User';
import { UserService } from 'src/app/services/user.service';
import { ifError } from 'assert';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  faSmileWink = faSmileWink;
  
  user: User;
  userForm: FormGroup;

  constructor(
    private userService: UserService,
    private formBuilder: FormBuilder,
    private validateService: ValidateService,
    private authService: AuthService,
    private flashMsgService: NgFlashMessageService,
    private router: Router,
  ) {
    this.userForm = this.formBuilder.group({
      name: '',
      username: '',
      email: '',
      password: ''
    });
  }
  ngOnInit() {
    this.flashMsgService.showFlashMessage({
      messages: ['Please fill out the registration form =)'],
      dismissible: true,
      timeout: false,
      type: 'success'
    });
  }
  onSubmit() {
    // wire values
    this.user = this.userForm.value;
        
    ifError(this.flashMsgService.showFlashMessage({
      messages: ['Something went wrong...'],
      dismissible: true,
      timeout: false,
      type: 'warning'
    }));

    // Validate required fields
    if (!this.validateService.validateRegister(this.user)) {
      this.flashMsgService.showFlashMessage({
        messages: ['Please fill in all fields  D:'],
        dismissible: true,
        timeout: false,
        type: 'danger'
      });
      return false;
    }
    // Validate Email
    if (!this.validateService.validateEmail(this.user.email)) {
      this.flashMsgService.showFlashMessage({
        messages: ['Please use a valid email D:'],
        dismissible: true,
        timeout: false,
        type: 'danger'
      });
      return false;
    }
    // Register user
    this.authService.registerUser(this.user).subscribe((data: any) => {
      //TODO can't get data?
      console.log(data);
      if (data.success) {
        this.userForm.reset();
        this.flashMsgService.showFlashMessage({
          messages: ["Registration successful"],
          dismissible: true,
          timeout: false,
          type: 'success'
        });
        //this.router.navigate(['/login']); 
      
      
      } else {
        this.flashMsgService.showFlashMessage({
          messages: ['Registration sent !'],
          dismissible: true,
          timeout: false,
          type: 'success'
        });
        this.router.navigate(['/login']);
      }
    });
    this.userService.createUser(this.user);
  }
}
