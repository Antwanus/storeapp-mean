import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from "@angular/router";

import { NgFlashMessageService } from 'ng-flash-messages';
import { User } from 'src/app/models/User';
import { UserService } from 'src/app/services/user.service';
import { ifError } from 'assert';
import { AuthService } from 'src/app/services/auth.service';
import { stringify } from 'querystring';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user: User;
  loginForm: FormGroup;
  res: {};

  constructor(
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private flashMsgService: NgFlashMessageService,
    private router: Router,
  ) {
    this.loginForm = this.formBuilder.group({
      username: '',
      password: ''
    });
  }

  ngOnInit() {
    this.flashMsgService.showFlashMessage({
      messages: ['Please log in =)'],
      dismissible: true,
      timeout: 5000,
      type: 'info'
    });
  }

  onSubmit() {
    this.user = this.loginForm.value;
    let userIsLegit: boolean = false;

    this.authService
        .authenticateUserLogin(this.user)
        .subscribe((data: any) => {
          userIsLegit = data.succes;
          
          console.log(data);
          if (userIsLegit) {
            this.flashMsgService.showFlashMessage({
              messages: ['Access granted, welcome home!'],
              dismissible: false,
              timeout: false,
              type: 'success'
            });
            this.authService.storeUserData(data.token, data.user)
            this.loginForm.reset();
            this.router.navigate(['/dashboard']);
          } else {
            this.flashMsgService.showFlashMessage({
              messages: ['Getting an error...'],
              dismissible: true,
              timeout: false,
              type: 'warning'
            });
          }
        });
  }
}
