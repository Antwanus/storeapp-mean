import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { User } from 'src/app/models/User';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  user: User;
  constructor(
    private router: Router,
    private authService: AuthService,

  ) { }

  //TODO: not working because of duplicate 'Bearer Bearer 12345'
  ngOnInit() {
    // initialize user on init
    this.authService.getProfile().subscribe((profile: any) => {
      console.log(profile.user);
      this.user = profile.user;
    }
    // ,
    // err => {
    //   console.log(err);
    //   return false; 
    // }
    );
  }
}
