import { Component, OnInit } from '@angular/core';

import { SupplierService } from "../../services/supplier.service";
import { Supplier } from "../../models/Supplier";

import { FormBuilder, FormGroup } from "@angular/forms";
import { NgFlashMessageService } from 'ng-flash-messages';

@Component({
  selector: 'app-supplier-form',
  templateUrl: './supplier-form.component.html',
  styleUrls: ['./supplier-form.component.css']
})
export class SupplierFormComponent implements OnInit {

  supplier: Supplier;
  supplierForm: FormGroup;

  constructor(
    private supplierService: SupplierService,
    private formBuilder: FormBuilder,
    private flashMsg: NgFlashMessageService,
  ) {
    this.supplierForm = this.formBuilder.group({
      supplierName: '',
    });
  }

  ngOnInit() {  }
  
  ngAfterContentInit() {
    // welcome & instruct user
    this.flashMsg.showFlashMessage({
      messages: ['Please fill in the form.'],
      dismissible: true,
      timeout: 5000,
      type: 'info'
    });
  }

  // SUBMIT FORM
  onSubmit() {
    // wire Supplier to form data
    this.supplier = this.supplierForm.value;
    // reset the form to initial value (in constructor)
    this.supplierForm.reset();
    // call service for POST
    this.supplierService
            .createSupplier(this.supplier)
            .subscribe();

  }

}
