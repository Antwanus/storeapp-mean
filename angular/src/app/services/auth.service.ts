import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { JwtHelperService } from "@auth0/angular-jwt";
import { User } from '../models/User';

import { map } from "rxjs/operators";

const httpOptions = {
  headers: new HttpHeaders({
    "Content-Type": "application/json",
  })
};

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  registerUrl: string = 'http://localhost:3003/users/register';
  profileUrl: string = 'http://localhost:3003/users/profile';
  authenticateUrl: string = 'http://localhost:3003/users/authenticate';
  authToken: any;
  user: User;

  constructor(
    private http: HttpClient,
    private jwtHelper: JwtHelperService
  ) { }

  registerUser(user: User) {
    return this.http
      .post(this.registerUrl, user, httpOptions)
      .pipe(map(res => res));
  }
  authenticateUserLogin(user: User) {
    return this.http
      .post(this.authenticateUrl, user, httpOptions)
      .pipe(map(res => res));
  }
  getProfile() {
    this.loadToken();
    
    //BROKEN? - httpOptions.headers.append("Authorization", this.authToken);
    
    // THIS LOGS CORRECTLY    
    // let httpOptions2 = {
    //   headers: new HttpHeaders({
    //     "Content-Type": "application/json",
    //     "Authorization": this.authToken,
    //   })
    // };
    // console.log(httpOptions2.headers.getAll("Authorization"));
    
    let headers = new HttpHeaders();
    headers = headers.append('Authorization', this.authToken);
    headers = headers.append('Content-Type', 'application/json');
    console.log(headers);
    
    return this.http
      .post(this.profileUrl, {headers: headers, withCredentials: true})
      .pipe(map(res => res));
      
  }
  storeUserData(token, user){
    localStorage.setItem('id_token', token);
    // local storage can only take in strings
    localStorage.setItem('user', JSON.stringify(user));

    this.authToken = token;
    this.user = user;
  }
  loadToken(): string{
    const token = localStorage.getItem('id_token');
    this.authToken = token;
    return token
  }
  logout(){
    this.authToken = null;
    this.user = null;
    localStorage.clear();
  }
  userIsLoggedOut(){
    return this.jwtHelper.isTokenExpired(this.authToken);
  }

}
