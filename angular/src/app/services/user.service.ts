import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../models/User';
import { map } from 'rxjs/operators';


const httpOptions = {
  headers: new HttpHeaders({
    "Content-Type": "application/json",
    "X-Forwarded-Host": "localhost",
    "X-Forwarded-Port": "4200"
  })
};

@Injectable({
  providedIn: 'root'
})
export class UserService {
  userUrl: string = "http://localhost:3003/users/register";

  constructor(private http: HttpClient) { }

  createUser(user: User): Observable<User>{
    return this.http
        .post(this.userUrl, user, httpOptions)
        .pipe(map((result: any) => {
          return result;
        }))
  }
}
