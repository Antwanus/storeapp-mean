import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Observable } from "rxjs";

import { Product } from "../models/Product";
import { Supplier } from '../models/Supplier';

const httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/hal+json",
      "X-Forwarded-Host": "localhost",
      "X-Forwarded-Port": "4200"
    })
};

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  productsUrl: string = "api/products";
  size: string = "?size=250";
  fullUrl: string = this.productsUrl + this.size;

  constructor(private http: HttpClient) { }

  // GET ALL THE PRODUCTS
  getAllProducts(): Observable<Product[]> {
    return this.http
      .get(this.fullUrl, httpOptions)
      .pipe(map((result: any) => {
        return result._embedded.products;
    }));
  }
  // GET SINGLE PRODUCT
  getSingleProduct(product: Product): Observable<Product> {
    const url: string = `${this.productsUrl}/${product.productId}`;
    return this.http
      .get(url, httpOptions)
      .pipe(map((result: any) => {
        return result;
      }));
  }
  // GET PRODUCT-SUPPLIERS
  getProductSuppliers(product: Product): Observable<Supplier[]>{
    const url: string = `${this.productsUrl}/${product.productId}/supplier`;
    return this.http
    .get(url, httpOptions)
    .pipe(map((result: any) => {
      return result._embedded.suppliers;
    }));
  }
  // DELETE PRODUCT BY ID
  deleteProductById(product: Product){
    const url: string = `${this.productsUrl}/${product.productId}`;
    return this.http.delete(url, httpOptions);
  }
  // DELETE PRODUCT BY 'href'
  deleteProductByHref(product: Product) {
    return this.http.delete(product._links.self.href, httpOptions);

  }
  // CREATE NEW PRODUCT
  createProduct(product: Product): Observable<Product> {
    return this.http.post(this.productsUrl, product, httpOptions);
  }

}
