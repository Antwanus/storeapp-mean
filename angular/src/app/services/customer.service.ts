import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpParams } from "@angular/common/http";
import { map, retry, catchError } from "rxjs/operators";
import { Observable, throwError } from "rxjs";

import { Customer } from "../models/Customer";

@Injectable({ providedIn: 'root' })
export class CustomerService {
  customersUrl: string = "api/customers";

  constructor(
    private http: HttpClient
  ) { }

  // GET ALL CUSTOMERS
  getAllCustomers(): Observable<Customer[]> {
    /** WORKING WITH INTERCEPTORS NOW
    // immutable object
    const headers = new HttpHeaders({
      "Content-Type": "application/hal+json",
      "X-Forwarded-Host": "localhost",
      "X-Forwarded-Port": "4200"
    });
    // appending to an immutable object
    // https://specs.openstack.org/openstack/api-wg/guidelines/pagination_filter_sort.html
    let params = new HttpParams().append('size', '100');
    params = params.append('sort', 'customerName');
    */
    return this.http
      .get(this.customersUrl
      //    , {headers: headers, params: params}  
      )
      .pipe(
        // try to get it 3 times before catching the error
        retry(3),
        catchError(this.handleError),
        map((result: any) => {
        return result._embedded.customers;
      }));
  }

  // CREATE A CUSTOMER
  createCustomer(customer: Customer): Observable<Customer> {
    return this.http.post<Customer>(this.customersUrl, customer);
  }

  // basic dummy handle error
  handleError(error: HttpErrorResponse) {
    console.log(error);
    return throwError(error);
    
  }
  
}
