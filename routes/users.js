const express = require('express');
const router = express.Router();

const passport = require('passport');
const jwt = require('jsonwebtoken');

const config = require('../config/db')
const User = require('../models/user');



// REGISTER
router.post('/register', (req, res, next) => {
    // parse the input to a User class
    let newUser = new User({
        name: req.body.name,
        email: req.body.email,
        username: req.body.username,
        password: req.body.password
    });
    // send to DB & callback
    User.addUser(newUser, (err, user) => {
        if(err) {
            // res.status(404).json("Can't find this :O")
            // res.status(500).json("Oh-oh... :A")
            res.json({
                succes: false,
                msg: 'Registration failed! D:'
            });
        } else {
            res.json({
                succes: true,
                msg: 'Welcome to the party! :D'
            });
        }
    })
});

// AUTHENTICATE
router.post('/authenticate', (req, res, next) => {
    const username = req.body.username;
    const password = req.body.password;

    // check if username exists
    User.getUserByUsername(username, (err, user) => {
        if(err) throw err;
        if(!user) {
            return res.json({
                succes: false, 
                msg: "You shall not pass <):D"
            });
        }
        // compare password (hash)
        User.comparePassword(password, user.password, (err, isMatch) => {
            if(err) throw err;
            if(isMatch) {
                const token = jwt.sign({ data: user }, config.secret, {
                    expiresIn: 604800 // 1 week :D
                });
                // succes
                res.json({
                    userObj: user,
                    succes: true,
                    token: 'Bearer ' +token,
                    user: {
                        id: user._id,
                        name: user.name,
                        username: user.username,
                        email: user.email
                    }
                });
                // fail
                } else {
                    return res.json({
                        succes: false, 
                        msg: "You shall not pass with that word... <):P"
                    });
                }
            
        })
    })
});

// TOP SECRET PROFILE :D
router.get('/profile', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    res.json({ user: req.user });
});
module.exports = router;