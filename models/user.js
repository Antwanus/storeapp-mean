const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/db');

/** SCHEMA */
const UserSchema = mongoose.Schema({
    name: {
        type: String
    },
    email: {
        type: String,
        required: true
    },
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    }
});

/** 'DAO' */
const User = module.exports = mongoose.model('User', UserSchema);

/** 'REPO' */
module.exports.getUserById = function(id, callback) {
    User.findById(id, callback);
}
module.exports.getUserByUsername = function(username, callback) {
    const query = { username: username };
    User.findOne(query, callback);
}
module.exports.addUser = function (newUser, callback) {
    // hash the PW
    bcrypt.genSalt(10, (err, salt) => {
        if (err) throw err;
        bcrypt.hash(newUser.password, salt, (err, hash) => {
            if (err) throw err;
            newUser.password = hash;
            newUser.save(callback);
        });
    });
}
module.exports.comparePassword = function (recievedPassword, hash, callback) {
    // async compare data
    bcrypt.compare(recievedPassword, hash, (err, isMatch) => {
        if (err) throw err;
        callback(null, isMatch);
    });
}